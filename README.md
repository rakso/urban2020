# Ściąga HTML
Szablon strony, to:
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Tytuł strony</title>
        <!-- Tu umieszczane są metadane strony -->
    </head>
    <body>
        <!-- Tu jest główna zawartość strony -->
    </body>
</html>
```
## Formatowanie tekstu
### Nagłówek `<hX><hX/>`
X to stopień wielkości. 1 (największy) do 6 (najmniejszy), np `<h1>Nagłówek stopnia pierwszego<h1>`

### Akapit `<p></p>`
Test
